Gogomba quick start guide
---

# Configuration

- Using the configuration webpage enter the connection details

    Currently only http connections are supported. The Pebble javascript sdk has problems dealing with isy's self signed certificates. So this only works from 
    inside your network (assuming you did not forward unsecured webaccess)

![server settings](./images/gogomba_conf_2.PNG)

- Add a switch
   Works also great with scenes. Sample uses a scene. 
   Only thing needed is the scence id

![scene setup](./images/gogomba_conf_3.PNG)

- Save

# On the Pebble

- Select the scene you want to control
![scene control](./images/gogomba1.png)

- Turn it on or off with the up and down buttons
![scent control](./images/gogomba2.png)

# New - Control Status
Switches and scenes now show their current status (or an error code if something went wrong)

This only works for scenes is a scene has a controller. If this is not the case there is no way to determine if a scene is on or off.

